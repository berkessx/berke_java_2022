public class Main {

    public static void main(String[] args) {
        class Solution {
            public ListNode deleteDuplicates(ListNode head) {
                ListNode temp=head;
                if(head==null)
                    return null;
                while(temp.next!=null)
                {
                    if(temp.val==temp.next.val)
                    {
                        ListNode next=temp.next.next;
                        temp.next=next;
                    }
                    else
                    {
                        temp=temp.next;
                    }
                }
                return head;

            }
        }
    }
}
